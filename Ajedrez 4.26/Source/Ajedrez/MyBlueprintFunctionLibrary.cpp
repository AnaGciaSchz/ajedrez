// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBlueprintFunctionLibrary.h"

UMyBlueprintFunctionLibrary::UMyBlueprintFunctionLibrary() {
	initialise_all_databases();
	zobrist::initialise_zobrist_keys();
}

FString UMyBlueprintFunctionLibrary::GetInitialMoves() {
	ChessPosition p;
	ChessPosition::set("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1", p);

	SurgeMoveList<SURGEWHITE> list(p);

	std::string tmp = "";

	for (ChessMove m : list) {
		tmp += to_string(m) + " ";
	}

	FString ret(tmp.c_str());

	return ret;
}

FString UMyBlueprintFunctionLibrary::GetMovesFor(int color, FString position) {
	ChessPosition p;
	ChessPosition::set(TCHAR_TO_UTF8(*position), p);

	if (color == 0) {
		SurgeMoveList<SURGEWHITE> list(p);

		std::string tmp = "";

		for (ChessMove m : list) {
			tmp += to_string(m) + " ";
		}

		FString ret(tmp.c_str());

		return ret;
	}
	else {
		SurgeMoveList<SURGEBLACK> list(p);

		std::string tmp = "";

		for (ChessMove m : list) {
			tmp += to_string(m) + " ";
		}

		FString ret(tmp.c_str());

		return ret;
	}
}

namespace PSQT {
	void init();
}

const char* StartFEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

FString UMyBlueprintFunctionLibrary::StockFishMove(FString currentPosition) {

	UCI::init(Options);
	Tune::init();
	PSQT::init();
	Bitboards::init();
	Position::init();
	Bitbases::init();
	Endgames::init();
	StockFishThreads.set(size_t(Options["StockFishThreads"]));
	Search::clear(); // After threads are up

	Position pos;
	std::string cmd;
	StateListPtr states(new std::deque<StateInfo>(1));

	pos.set("rnbqkbnr/pp1ppppp/8/2p5/4P3/5N2/PPPP1PPP/RNBQKB1R b KQkq - 1 2", false, &states->back(), StockFishThreads.main());

	cmd = "startpos";
	std::istringstream is(cmd);

	UCI::UciPosition(pos, is, states);

	cmd = "depth 15";
	is.str(cmd);

	//UCI::UciGo(pos, is, states);

	return UTF8_TO_TCHAR(bestMoveRet.c_str());
}

FString UMyBlueprintFunctionLibrary::UpdateFEN(FString initialFEN, FString move) { // d2d3

	// 1. get variables ofen and mtbm
	string ofen = TCHAR_TO_UTF8(*initialFEN);
	string mtbm = TCHAR_TO_UTF8(*move);

	// 2. separate mtbm into loc, des, and pp
	string loc, des;
	char pp;
	int locrow, desrow;

	if (mtbm.size() >= 4)
	{
		locrow = 7 - (mtbm[1] - '1');
		des = mtbm[2] + mtbm[3];
		desrow = 7 - (mtbm[3] - '1');
		if (mtbm.size() >= 5)
			pp = mtbm[4];
	}

	// 3. identify segments in ofen related to loc and des
	vector<string> ofensplit = split(ofen, ' ');
	string ofentrunc = ofensplit[0];
	
	vector<string> ofentruncsplit = split(ofentrunc, '/');
	
	string ofenloc = ofentruncsplit[locrow];
	string ofendes = ofentruncsplit[desrow];

	// 4. expand integers in segments to rows of ones
	string ofenlocexp = "";
	for (auto it = ofenloc.cbegin(); it != ofenloc.cend(); ++it) {
		if ('1' <= *it && *it <= '9')
		{
			int l = *it - '0';
			for (int i = 0; i < l; i++)
			{
				ofenlocexp.push_back('1');
			}
		}
		else
			ofenlocexp.push_back(*it);
	}

	string ofendesexp = "";
	if (locrow != desrow) {
		for (auto it = ofendes.cbegin(); it != ofendes.cend(); ++it) {
			if ('1' <= *it && *it <= '9')
			{
				int l = *it - '0';
				for (int i = 0; i < l; i++)
				{
					ofendesexp.push_back('1');
				}
			}
			else
				ofendesexp.push_back(*it);
		}
	}

	// 5. identify relevant characters in loc and des
	char pieceloc = ofenlocexp[mtbm[0] - 'a'];
	char piecedes;
	if (locrow != desrow) {
		piecedes = ofendesexp[mtbm[2] - 'a'];
	}
	else {
		piecedes = ofenlocexp[mtbm[2] - 'a'];
	}

	if ((pieceloc != 'K' && mtbm.compare("e1g1") != 0 && mtbm.compare("e1b1") != 0) // white castling
		|| (pieceloc != 'k' && mtbm.compare("e8g8") != 0 && mtbm.compare("e8b8") != 0)) // black castling
	{
		// 6. remove piece character from ofenlocexp and replace with 1
		ofenlocexp[mtbm[0] - 'a'] = '1';

		// 7. replace 1 or piece character in des with loc character or pp if applicable
		if (locrow != desrow) {
			if (mtbm.size() >= 5)
				ofendesexp[mtbm[2] - 'a'] = pp;
			else
				ofendesexp[mtbm[2] - 'a'] = pieceloc;
		}
		else {
			if (mtbm.size() >= 5)
				ofenlocexp[mtbm[2] - 'a'] = pp;
			else
				ofenlocexp[mtbm[2] - 'a'] = pieceloc;
		}
	}
	else {
		// 6-7b. Castling move
		if (mtbm.compare("e1g1") == 0)	// white short castling
		{
			ofenlocexp[mtbm[0] - 'a'] = '1';	// prev king position
			ofenlocexp[mtbm[2] - 'a'] = pieceloc;	// new king position
			ofenlocexp[7] = '1';	// prev rook position
			ofenlocexp[5] = 'R';	// new rook position
		}
		else if (mtbm.compare("e1b1") == 0)	// white long castling
		{
			ofenlocexp[mtbm[0] - 'a'] = '1';	// prev king position
			ofenlocexp[mtbm[2] - 'a'] = pieceloc;	// new king position
			ofenlocexp[0] = '1';	// prev rook position
			ofenlocexp[3] = 'R';	// new rook position
		}
		else if (mtbm.compare("e8g8") == 0)	// black short castling
		{
			ofenlocexp[mtbm[0] - 'a'] = '1';	// prev king position
			ofenlocexp[mtbm[2] - 'a'] = pieceloc;	// new king position
			ofenlocexp[7] = '1';	// prev rook position
			ofenlocexp[5] = 'r';	// new rook position
		}
		else if (mtbm.compare("e8b8") == 0)	// black long castling
		{
			ofenlocexp[mtbm[0] - 'a'] = '1';	// prev king position
			ofenlocexp[mtbm[2] - 'a'] = pieceloc;	// new king position
			ofenlocexp[0] = '1';	// prev rook position
			ofenlocexp[3] = 'r';	// new rook position
		}
	}

	// 8. contract segments to merge all the ones
	int counter = 0;
	string ofenlocnew = "";
	for (auto it = ofenlocexp.cbegin(); it != ofenlocexp.cend(); ++it) {
		if (*it == '1')
			counter++;
		else
		{
			if (counter > 0)
				ofenlocnew.push_back(counter + '0');
			ofenlocnew.push_back(*it);
			counter = 0;
		}
	}
	if (counter > 0)
		ofenlocnew.push_back(counter + '0');

	string ofendesnew = "";
	if (locrow != desrow) {
		counter = 0;
		for (auto it = ofendesexp.cbegin(); it != ofendesexp.cend(); ++it) {
			if (*it == '1')
				counter++;
			else
			{
				if (counter > 0)
					ofendesnew.push_back(counter + '0');
				ofendesnew.push_back(*it);
				counter = 0;
			}
		}
		if (counter > 0)
			ofendesnew.push_back(counter + '0');
	}

	// 9. invert color to move
	string newcolor = ( ofensplit[1][0] == 'w' ? "b" : "w" );

	//10. modify castling rigths
	string castling = "";
	if(ofensplit[2][0] != '-')
	{
		int posK = ofensplit[2].find("K");
		if (posK >= 0 && pieceloc != 'K' && (mtbm[0] - 'a' != 7 || mtbm[1] - '1' != 0)) // lost the rigth to K, either moving white king or right rook
			castling.push_back('K');
		int posQ = ofensplit[2].find("Q");
		if (posQ >= 0 && pieceloc != 'K' && (mtbm[0] - 'a' != 0 || mtbm[1] - '1' != 0)) // lost the rigth to Q, either moving white king or left rook
			castling.push_back('Q');
		int posk = ofensplit[2].find("k");
		if (posk >= 0 && pieceloc != 'k' && (mtbm[0] - 'a' != 7 || mtbm[1] - '1' != 7)) // lost the rigth to k, either moving black king or right rook
			castling.push_back('k');
		int posq = ofensplit[2].find("q");
		if (posq >= 0 && pieceloc != 'k' && (mtbm[0] - 'a' != 0 || mtbm[1] - '1' != 7)) // lost the rigth to q, either moving black king or left rook
			castling.push_back('q');
	}
	if(castling.size() == 0)
		castling.push_back('-');

	//11. modify en passant
	string passant = "";

	if (pieceloc == 'P' && ((mtbm[3] - mtbm[1]) > 1)) // En passant white
	{
		passant.push_back(mtbm[0]);
		passant.push_back(mtbm[1] + 1);
	}

	if (pieceloc == 'p' && ((mtbm[1] - mtbm[3]) > 1)) // En passant black
	{
		passant.push_back(mtbm[0]);
		passant.push_back(mtbm[1] - 1);
	}

	if (passant.size() == 0)
		passant.push_back('-');

	//12. modify halfmove and fullmove
	int halfmoves = stoi(ofensplit[4]);
	if (pieceloc == 'p' || pieceloc == 'P' || piecedes != '1')
	{
		halfmoves = 0;
	}
	else
		halfmoves++;

	int moves = stoi(ofensplit[5]);
	if (ofensplit[1][0] == 'b')
		moves++;

	//13. Oh, yeah. It's all coming together.
	ofentruncsplit[locrow] = ofenlocnew;
	if(locrow != desrow)
		ofentruncsplit[desrow] = ofendesnew;

	string ret = "";

	for (const auto& row : ofentruncsplit) ret += row + "/";

	ret = ret.substr(0, ret.size() - 1);

	ret += " " + newcolor + " " + castling + " " + passant;
	ret += " " + to_string(halfmoves) + " " + to_string(moves);

	FString fret(ret.c_str());
	return fret;
}

FString UMyBlueprintFunctionLibrary::GetPieceColor(FString currentFEN, FString coord) { // 'w' if white 'b' if black "" else
	// 1. get variables ofen and mtbm
	string ofen = TCHAR_TO_UTF8(*currentFEN);
	string mtbm = TCHAR_TO_UTF8(*coord);

	// 2. separate mtbm into loc
	int locrow;

	if (mtbm.size() >= 2)
	{
		locrow = 7 - (mtbm[1] - '1');
	}

	if (locrow < 0 || locrow > 7)
	{
		string ret = "";
		FString fret(ret.c_str());
		return fret;
	}

	// 3. identify segments in ofen related to loc
	vector<string> ofensplit = split(ofen, ' ');
	string ofentrunc = ofensplit[0];

	vector<string> ofentruncsplit = split(ofentrunc, '/');

	string ofenloc = ofentruncsplit[locrow];

	// 4. expand integers in segments to rows of ones
	string ofenlocexp = "";
	for (auto it = ofenloc.cbegin(); it != ofenloc.cend(); ++it) {
		if ('1' <= *it && *it <= '9')
		{
			int l = *it - '0';
			for (int i = 0; i < l; i++)
			{
				ofenlocexp.push_back('1');
			}
		}
		else
			ofenlocexp.push_back(*it);
	}

	// 5. identify relevant characters in loc and des
	char pieceloc = ofenlocexp[mtbm[0] - 'a'];

	string ret = "";
	if (pieceloc >= 'A' && pieceloc <= 'Z')
		ret = "w";
	if (pieceloc >= 'a' && pieceloc <= 'z')
		ret = "b";

	FString fret(ret.c_str());
	return fret;
}

vector<string> UMyBlueprintFunctionLibrary::split(const string& s, char delim) {
	vector<string> result;
	stringstream ss(s);
	string item;

	while (getline(ss, item, delim)) {
		result.push_back(item);
	}

	return result;
}
