#pragma once

#include "types.h"

extern const SurgeBitboard KING_ATTACKS[NSQUARES];
extern const SurgeBitboard KNIGHT_ATTACKS[NSQUARES];
extern const SurgeBitboard WHITE_PAWN_ATTACKS[NSQUARES];
extern const SurgeBitboard BLACK_PAWN_ATTACKS[NSQUARES];

extern SurgeBitboard reverse(SurgeBitboard b);
extern SurgeBitboard sliding_attacks(SurgeSquare square, SurgeBitboard occ, SurgeBitboard mask);

extern SurgeBitboard get_rook_attacks_for_init(SurgeSquare square, SurgeBitboard occ);
extern const SurgeBitboard ROOK_MAGICS[NSQUARES];
extern SurgeBitboard ROOK_ATTACK_MASKS[NSQUARES];
extern int ROOK_ATTACK_SHIFTS[NSQUARES];
extern SurgeBitboard ROOK_ATTACKS[NSQUARES][4096];
extern void initialise_rook_attacks();


extern constexpr SurgeBitboard get_rook_attacks(SurgeSquare square, SurgeBitboard occ);
extern SurgeBitboard get_xray_rook_attacks(SurgeSquare square, SurgeBitboard occ, SurgeBitboard blockers);

extern SurgeBitboard get_bishop_attacks_for_init(SurgeSquare square, SurgeBitboard occ);
extern const SurgeBitboard BISHOP_MAGICS[NSQUARES];
extern SurgeBitboard BISHOP_ATTACK_MASKS[NSQUARES];
extern int BISHOP_ATTACK_SHIFTS[NSQUARES];
extern SurgeBitboard BISHOP_ATTACKS[NSQUARES][512];
extern void initialise_bishop_attacks();


extern constexpr SurgeBitboard get_bishop_attacks(SurgeSquare square, SurgeBitboard occ);
extern SurgeBitboard get_xray_bishop_attacks(SurgeSquare square, SurgeBitboard occ, SurgeBitboard blockers);

extern SurgeBitboard SQUARES_BETWEEN_BB[NSQUARES][NSQUARES];
extern SurgeBitboard LINE[NSQUARES][NSQUARES];
extern SurgeBitboard PAWN_ATTACKS[NCOLORS][NSQUARES];
extern SurgeBitboard PSEUDO_LEGAL_ATTACKS[NPIECE_TYPES][NSQUARES];

extern void initialise_squares_between();
extern void initialise_line();
extern void initialise_pseudo_legal();
extern void initialise_all_databases();


//Returns a bitboard containing all squares that a piece on a square can chessMove to, in the given position
template<SurgePieceType P>
constexpr SurgeBitboard attacks(SurgeSquare s, SurgeBitboard occ) {
	static_assert(P != SURGEPAWN, "The piece type may not be a pawn; use pawn_attacks instead");
	return P == SURGEROOK ? get_rook_attacks(s, occ) :
		P == SURGEBISHOP ? get_bishop_attacks(s, occ) :
		P == SURGEQUEEN ? attacks<SURGEROOK>(s, occ) | attacks<SURGEBISHOP>(s, occ) :
		PSEUDO_LEGAL_ATTACKS[P][s];
}

//Returns a bitboard containing all squares that a piece on a square can chessMove to, in the given position
//Used when the piece type is not known at compile-time
constexpr SurgeBitboard attacks(SurgePieceType pt, SurgeSquare s, SurgeBitboard occ) {
	switch (pt) {
	case SURGEROOK:
		return attacks<SURGEROOK>(s, occ);
	case SURGEBISHOP:
		return attacks<SURGEBISHOP>(s, occ);
	case SURGEQUEEN:
		return attacks<SURGEQUEEN>(s, occ);
	default:
		return PSEUDO_LEGAL_ATTACKS[pt][s];
	}
}

//Returns a bitboard containing pawn attacks from all pawns in the given bitboard
template<SurgeColor C>
constexpr SurgeBitboard pawn_attacks(SurgeBitboard p) {
	return C == SURGEWHITE ? shift<SURGENORTH_WEST>(p) | shift<SURGENORTH_EAST>(p) :
		shift<SURGESOUTH_WEST>(p) | shift<SURGESOUTH_EAST>(p);
}

//Returns a bitboard containing pawn attacks from the pawn on the given square
template<SurgeColor C>
constexpr SurgeBitboard pawn_attacks(SurgeSquare s) {
	return PAWN_ATTACKS[C][s];
}
