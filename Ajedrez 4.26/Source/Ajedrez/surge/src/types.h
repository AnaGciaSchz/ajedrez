#pragma once
#pragma warning(disable : 26812)

#include <cstdint>
#include <string>
#include <ostream>
#include <iostream>
#include <vector>

const size_t NCOLORS = 2;
enum SurgeColor : int {
	SURGEWHITE, SURGEBLACK
};

//Inverts the color (SURGEWHITE -> SURGEBLACK) and (SURGEBLACK -> SURGEWHITE)
constexpr SurgeColor operator~(SurgeColor c) {
	return SurgeColor(c ^ SURGEBLACK);
}

const size_t NDIRS = 8;
enum SurgeDirection : int {
	SURGENORTH = 8, SURGENORTH_EAST = 9, SURGEEAST = 1, SURGESOUTH_EAST = -7,
	SURGESOUTH = -8, SURGESOUTH_WEST = -9, SURGEWEST = -1, SURGENORTH_WEST = 7,
	SURGENORTH_NORTH = 16, SURGESOUTH_SOUTH = -16
};

const size_t NPIECE_TYPES = 6;
enum SurgePieceType : int {
	SURGEPAWN, SURGEKNIGHT, SURGEBISHOP, SURGEROOK, SURGEQUEEN, SURGEKING
};

//PIECE_STR[piece] is the algebraic chess representation of that piece
const std::string PIECE_STR = "PNBRQK~>pnbrqk.";

//The FEN of the starting position
const std::string DEFAULT_FEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq -";

//The Kiwipete position, used for perft debugging
const std::string KIWIPETE = "r3k2r/p1ppqpb1/bn2pnp1/3PN3/1p2P3/2N2Q1p/PPPBBPPP/R3K2R w KQkq -";


const size_t NPIECES = 15;
enum SurgePiece : int {
	WHITE_PAWN, WHITE_KNIGHT, WHITE_BISHOP, WHITE_ROOK, WHITE_QUEEN, WHITE_KING,
	BLACK_PAWN = 8, BLACK_KNIGHT, BLACK_BISHOP, BLACK_ROOK, BLACK_QUEEN, BLACK_KING,
	SURGENO_PIECE
};

constexpr SurgePiece make_piece(SurgeColor c, SurgePieceType pt) {
	return SurgePiece((c << 3) + pt);
}

constexpr SurgePieceType type_of(SurgePiece pc) {
	return SurgePieceType(pc & 0b111);
}

constexpr SurgeColor color_of(SurgePiece pc) {
	return SurgeColor((pc & 0b1000) >> 3);
}



typedef uint64_t SurgeBitboard;

const size_t NSQUARES = 64;
enum SurgeSquare : int {
	a1, b1, c1, d1, e1, f1, g1, h1,
	a2, b2, c2, d2, e2, f2, g2, h2,
	a3, b3, c3, d3, e3, f3, g3, h3,
	a4, b4, c4, d4, e4, f4, g4, h4,
	a5, b5, c5, d5, e5, f5, g5, h5,
	a6, b6, c6, d6, e6, f6, g6, h6,
	a7, b7, c7, d7, e7, f7, g7, h7,
	a8, b8, c8, d8, e8, f8, g8, h8,
	NO_SQUARE
};

inline SurgeSquare& operator++(SurgeSquare& s) { return s = SurgeSquare(int(s) + 1); }
constexpr SurgeSquare operator+(SurgeSquare s, SurgeDirection d) { return SurgeSquare(int(s) + int(d)); }
constexpr SurgeSquare operator-(SurgeSquare s, SurgeDirection d) { return SurgeSquare(int(s) - int(d)); }
inline SurgeSquare& operator+=(SurgeSquare& s, SurgeDirection d) { return s = s + d; }
inline SurgeSquare& operator-=(SurgeSquare& s, SurgeDirection d) { return s = s - d; }

enum SurgeFile : int {
	AFILE, BFILE, CFILE, DFILE, EFILE, FFILE, GFILE, HFILE
};	

enum SurgeRank : int {
	RANK1, RANK2, RANK3, RANK4, RANK5, RANK6, RANK7, RANK8
};

extern const char* SQSTR[65];

extern const SurgeBitboard MASK_FILE[8];
extern const SurgeBitboard MASK_RANK[8];
extern const SurgeBitboard MASK_DIAGONAL[15];
extern const SurgeBitboard MASK_ANTI_DIAGONAL[15];
extern const SurgeBitboard SQUARE_BB[65];

extern void print_bitboard(SurgeBitboard b);

extern const SurgeBitboard k1;
extern const SurgeBitboard k2;
extern const SurgeBitboard k4;
extern const SurgeBitboard kf;

extern inline int pop_count(SurgeBitboard x);
extern inline int sparse_pop_count(SurgeBitboard x);
extern inline SurgeSquare surge_pop_lsb(SurgeBitboard* b);

extern const int DEBRUIJN64[64];
extern const SurgeBitboard MAGIC;
extern constexpr SurgeSquare bsf(SurgeBitboard b);

constexpr SurgeRank rank_of(SurgeSquare s) { return SurgeRank(s >> 3); }
constexpr SurgeFile file_of(SurgeSquare s) { return SurgeFile(s & 0b111); }
constexpr int diagonal_of(SurgeSquare s) { return 7 + rank_of(s) - file_of(s); }
constexpr int anti_diagonal_of(SurgeSquare s) { return rank_of(s) + file_of(s); }
constexpr SurgeSquare create_square(SurgeFile f, SurgeRank r) { return SurgeSquare(r << 3 | f); }

//Shifts a bitboard in a particular direction. There is no wrapping, so bits that are shifted of the edge are lost 
template<SurgeDirection D>
constexpr SurgeBitboard shift(SurgeBitboard b) {
	return D == SURGENORTH ? b << 8 : D == SURGESOUTH ? b >> 8
		: D == SURGENORTH + SURGENORTH ? b << 16 : D == SURGESOUTH + SURGESOUTH ? b >> 16
		: D == SURGEEAST ? (b & ~MASK_FILE[HFILE]) << 1 : D == SURGEWEST ? (b & ~MASK_FILE[AFILE]) >> 1
		: D == SURGENORTH_EAST ? (b & ~MASK_FILE[HFILE]) << 9 
		: D == SURGENORTH_WEST ? (b & ~MASK_FILE[AFILE]) << 7
		: D == SURGESOUTH_EAST ? (b & ~MASK_FILE[HFILE]) >> 7 
		: D == SURGESOUTH_WEST ? (b & ~MASK_FILE[AFILE]) >> 9
		: 0;	
}

//Returns the actual rank from a given side's perspective (e.g. rank 1 is rank 8 from Black's perspective)
template<SurgeColor C>
constexpr SurgeRank relative_rank(SurgeRank r) {
	return C == SURGEWHITE ? r : SurgeRank(RANK8 - r);
}

//Returns the actual direction from a given side's perspective (e.g. North is South from Black's perspective)
template<SurgeColor C>
constexpr SurgeDirection relative_dir(SurgeDirection d) {
	return SurgeDirection(C == SURGEWHITE ? d : -d);
}

//The type of the chessMove
enum MoveFlags : int {
	QUIET = 0b0000, DOUBLE_PUSH = 0b0001,
	OO = 0b0010, OOO = 0b0011,
	CAPTURE = 0b1000,
	SURGECAPTURES = 0b1111,
	EN_PASSANT = 0b1010,
	PROMOTIONS = 0b0111,
	PROMOTION_CAPTURES = 0b1100,
	PR_KNIGHT = 0b0100, PR_BISHOP = 0b0101, PR_ROOK = 0b0110, PR_QUEEN = 0b0111,
	PC_KNIGHT = 0b1100, PC_BISHOP = 0b1101, PC_ROOK = 0b1110, PC_QUEEN = 0b1111,
};


class ChessMove {
private:
	//The internal representation of the chessMove
	uint16_t chessMove;
public:
	//Defaults to a null chessMove (a1a1)
	inline ChessMove() : chessMove(0) {}
	
	inline ChessMove(uint16_t m) { chessMove = m; }

	inline ChessMove(SurgeSquare from, SurgeSquare to) : chessMove(0) {
		chessMove = (from << 6) | to;
	}

	inline ChessMove(SurgeSquare from, SurgeSquare to, MoveFlags flags) : chessMove(0) {
		chessMove = (flags << 12) | (from << 6) | to;
	}

	ChessMove(const std::string& chessMove) {
		this->chessMove = (create_square(SurgeFile(chessMove[0] - 'a'), SurgeRank(chessMove[1] - '1')) << 6) |
			create_square(SurgeFile(chessMove[2] - 'a'), SurgeRank(chessMove[3] - '1'));
	}

	inline SurgeSquare to() const { return SurgeSquare(chessMove & 0x3f); }
	inline SurgeSquare from() const { return SurgeSquare((chessMove >> 6) & 0x3f); }
	inline int to_from() const { return chessMove & 0xffff; }
	inline MoveFlags flags() const { return MoveFlags((chessMove >> 12) & 0xf); }

	inline bool is_capture() const {
		return (chessMove >> 12) & SURGECAPTURES;
	}

	void operator=(ChessMove m) { chessMove = m.chessMove; }
	bool operator==(ChessMove a) const { return to_from() == a.to_from(); }
	bool operator!=(ChessMove a) const { return to_from() != a.to_from(); }
};

//Adds, to the chessMove pointer all moves of the form (from, s), where s is a square in the bitboard to
template<MoveFlags F = QUIET>
inline ChessMove* make(SurgeSquare from, SurgeBitboard to, ChessMove* list) {
	while (to) *list++ = ChessMove(from, surge_pop_lsb(&to), F);
	return list;
}

//Adds, to the chessMove pointer all quiet promotion moves of the form (from, s), where s is a square in the bitboard to
template<>
inline ChessMove *make<PROMOTIONS>(SurgeSquare from, SurgeBitboard to, ChessMove *list) {
	SurgeSquare p;
	while (to) {
		p = surge_pop_lsb(&to);
		*list++ = ChessMove(from, p, PR_KNIGHT);
		*list++ = ChessMove(from, p, PR_BISHOP);
		*list++ = ChessMove(from, p, PR_ROOK);
		*list++ = ChessMove(from, p, PR_QUEEN);
	}
	return list;
}

//Adds, to the chessMove pointer all capture promotion moves of the form (from, s), where s is a square in the bitboard to
template<>
inline ChessMove* make<PROMOTION_CAPTURES>(SurgeSquare from, SurgeBitboard to, ChessMove* list) {
	SurgeSquare p;
	while (to) {
		p = surge_pop_lsb(&to);
		*list++ = ChessMove(from, p, PC_KNIGHT);
		*list++ = ChessMove(from, p, PC_BISHOP);
		*list++ = ChessMove(from, p, PC_ROOK);
		*list++ = ChessMove(from, p, PC_QUEEN);
	}
	return list;
}

extern std::ostream& operator<<(std::ostream& os, const ChessMove& m);
extern std::string to_string(const ChessMove& m);

//The white king and kingside rook
const SurgeBitboard WHITE_OO_MASK = 0x90;
//The white king and queenside rook
const SurgeBitboard WHITE_OOO_MASK = 0x11;

//Squares between the white king and kingside rook
const SurgeBitboard WHITE_OO_BLOCKERS_AND_ATTACKERS_MASK = 0x60;
//Squares between the white king and queenside rook
const SurgeBitboard WHITE_OOO_BLOCKERS_AND_ATTACKERS_MASK = 0xe;

//The black king and kingside rook
const SurgeBitboard BLACK_OO_MASK = 0x9000000000000000;
//The black king and queenside rook
const SurgeBitboard BLACK_OOO_MASK = 0x1100000000000000;
//Squares between the black king and kingside rook
const SurgeBitboard BLACK_OO_BLOCKERS_AND_ATTACKERS_MASK = 0x6000000000000000;
//Squares between the black king and queenside rook
const SurgeBitboard BLACK_OOO_BLOCKERS_AND_ATTACKERS_MASK = 0xE00000000000000;

//The white king, white rooks, black king and black rooks
const SurgeBitboard ALL_CASTLING_MASK = 0x9100000000000091;

template<SurgeColor C> constexpr SurgeBitboard oo_mask() { return C == SURGEWHITE ? WHITE_OO_MASK : BLACK_OO_MASK; }
template<SurgeColor C> constexpr SurgeBitboard ooo_mask() { return C == SURGEWHITE ? WHITE_OOO_MASK : BLACK_OOO_MASK; }

template<SurgeColor C>
constexpr SurgeBitboard oo_blockers_mask() { 
	return C == SURGEWHITE ? WHITE_OO_BLOCKERS_AND_ATTACKERS_MASK :
		BLACK_OO_BLOCKERS_AND_ATTACKERS_MASK; 
}

template<SurgeColor C>
constexpr SurgeBitboard ooo_blockers_mask() {
	return C == SURGEWHITE ? WHITE_OOO_BLOCKERS_AND_ATTACKERS_MASK :
		BLACK_OOO_BLOCKERS_AND_ATTACKERS_MASK;
}
	
template<SurgeColor C> constexpr SurgeBitboard ignore_ooo_danger() { return C == SURGEWHITE ? 0x2 : 0x200000000000000; }
