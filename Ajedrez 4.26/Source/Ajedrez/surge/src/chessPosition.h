#pragma once

#include "types.h"
#include <ostream>
#include <string>
#include "tables.h"
#include <utility>

//A psuedorandom number generator
//Source: Stockfish
class SURGEPRNG {
	uint64_t s;

	uint64_t rand64() {
		s ^= s >> 12, s ^= s << 25, s ^= s >> 27;
		return s * 2685821657736338717LL;
	}

public:
	SURGEPRNG(uint64_t seed) : s(seed) {}

	//Generate psuedorandom number
	template<typename T> T rand() { return T(rand64()); }

	//Generate psuedorandom number with only a few set bits
	template<typename T> 
	T sparse_rand() {
		return T(rand64() & rand64() & rand64());
	}
};


namespace zobrist {
	extern uint64_t zobrist_table[NPIECES][NSQUARES];
	extern void initialise_zobrist_keys();
}

//Stores ChessPosition information which cannot be recovered on undo-ing a chessMove
struct UndoInfo {
	//The bitboard of squares on which pieces have either moved from, or have been moved to. Used for castling
	//legality checks
	SurgeBitboard entry;
	
	//The piece that was captured on the last chessMove
	SurgePiece captured;
	
	//The en passant square. This is the square which pawns can chessMove to in order to en passant capture an enemy pawn that has 
	//double pushed on the previous chessMove
	SurgeSquare epsq;

	constexpr UndoInfo() : entry(0), captured(SURGENO_PIECE), epsq(NO_SQUARE) {}
	
	//This preserves the entry bitboard across moves
	UndoInfo(const UndoInfo& prev) : 
		entry(prev.entry), captured(SURGENO_PIECE), epsq(NO_SQUARE) {}
};

class ChessPosition {
private:
	//A bitboard of the locations of each piece
	SurgeBitboard piece_bb[NPIECES];
	
	//A mailbox representation of the board. Stores the piece occupying each square on the board
	SurgePiece board[NSQUARES];
	
	//The side whose turn it is to play next
	SurgeColor side_to_play;
	
	//The current game ply (depth), incremented after each chessMove 
	int game_ply;
	
	//The zobrist hash of the ChessPosition, which can be incrementally updated and rolled back after each
	//make/unmake
	uint64_t hash;
public:
	//The history of non-recoverable information
	UndoInfo history[256];
	
	//The bitboard of enemy pieces that are currently attacking the king, updated whenever generate_moves()
	//is called
	SurgeBitboard checkers;
	
	//The bitboard of pieces that are currently pinned to the king by enemy sliders, updated whenever 
	//generate_moves() is called
	SurgeBitboard pinned;
	
	
	ChessPosition() : piece_bb{ 0 }, board{}, side_to_play(SURGEWHITE), game_ply(0),
		hash(0), checkers(0), pinned(0) {
		
		//Sets all squares on the board as empty
		for (int i = 0; i < 64; i++) board[i] = SURGENO_PIECE;
		history[0] = UndoInfo();
	}
	
	//Places a piece on a particular square and updates the hash. Placing a piece on a square that is 
	//already occupied is an error
	inline void put_piece(SurgePiece pc, SurgeSquare s) {
		board[s] = pc;
		piece_bb[pc] |= SQUARE_BB[s];
		hash ^= zobrist::zobrist_table[pc][s];
	}

	//Removes a piece from a particular square and updates the hash. 
	inline void remove_piece(SurgeSquare s) {
		hash ^= zobrist::zobrist_table[board[s]][s];
		piece_bb[board[s]] &= ~SQUARE_BB[s];
		board[s] = SURGENO_PIECE;
	}

	void move_piece(SurgeSquare from, SurgeSquare to);
	void move_piece_quiet(SurgeSquare from, SurgeSquare to);


	friend std::ostream& operator<<(std::ostream& os, const ChessPosition& p);
	static void set(const std::string& fen, ChessPosition& p);
	std::string fen() const;

	ChessPosition& operator=(const ChessPosition&) = delete;
	inline bool operator==(const ChessPosition& other) const { return hash == other.hash; }

	inline SurgeBitboard bitboard_of(SurgePiece pc) const { return piece_bb[pc]; }
	inline SurgeBitboard bitboard_of(SurgeColor c, SurgePieceType pt) const { return piece_bb[make_piece(c, pt)]; }
	inline SurgePiece at(SurgeSquare sq) const { return board[sq]; }
	inline SurgeColor turn() const { return side_to_play; }
	inline int ply() const { return game_ply; }
	inline uint64_t get_hash() const { return hash; }

	template<SurgeColor C> inline SurgeBitboard diagonal_sliders() const;
	template<SurgeColor C> inline SurgeBitboard orthogonal_sliders() const;
	template<SurgeColor C> inline SurgeBitboard all_pieces() const;
	template<SurgeColor C> inline SurgeBitboard attackers_from(SurgeSquare s, SurgeBitboard occ) const;

	template<SurgeColor C> inline bool in_check() const {
		return attackers_from<~C>(bsf(bitboard_of(C, SURGEKING)), all_pieces<SURGEWHITE>() | all_pieces<SURGEBLACK>());
	}

	template<SurgeColor C> void play(ChessMove m);
	template<SurgeColor C> void undo(ChessMove m);

	template<SurgeColor Us>
	ChessMove *generate_legals(ChessMove* list);
};

//Returns the bitboard of all bishops and queens of a given color
template<SurgeColor C> 
inline SurgeBitboard ChessPosition::diagonal_sliders() const {
	return C == SURGEWHITE ? piece_bb[WHITE_BISHOP] | piece_bb[WHITE_QUEEN] :
		piece_bb[BLACK_BISHOP] | piece_bb[BLACK_QUEEN];
}

//Returns the bitboard of all rooks and queens of a given color
template<SurgeColor C> 
inline SurgeBitboard ChessPosition::orthogonal_sliders() const {
	return C == SURGEWHITE ? piece_bb[WHITE_ROOK] | piece_bb[WHITE_QUEEN] :
		piece_bb[BLACK_ROOK] | piece_bb[BLACK_QUEEN];
}

//Returns a bitboard containing all the pieces of a given color
template<SurgeColor C> 
inline SurgeBitboard ChessPosition::all_pieces() const {
	return C == SURGEWHITE ? piece_bb[WHITE_PAWN] | piece_bb[WHITE_KNIGHT] | piece_bb[WHITE_BISHOP] |
		piece_bb[WHITE_ROOK] | piece_bb[WHITE_QUEEN] | piece_bb[WHITE_KING] :

		piece_bb[BLACK_PAWN] | piece_bb[BLACK_KNIGHT] | piece_bb[BLACK_BISHOP] |
		piece_bb[BLACK_ROOK] | piece_bb[BLACK_QUEEN] | piece_bb[BLACK_KING];
}

//Returns a bitboard containing all pieces of a given color attacking a particluar square
template<SurgeColor C> 
inline SurgeBitboard ChessPosition::attackers_from(SurgeSquare s, SurgeBitboard occ) const {
	return C == SURGEWHITE ? (pawn_attacks<SURGEBLACK>(s) & piece_bb[WHITE_PAWN]) |
		(attacks<SURGEKNIGHT>(s, occ) & piece_bb[WHITE_KNIGHT]) |
		(attacks<SURGEBISHOP>(s, occ) & (piece_bb[WHITE_BISHOP] | piece_bb[WHITE_QUEEN])) |
		(attacks<SURGEROOK>(s, occ) & (piece_bb[WHITE_ROOK] | piece_bb[WHITE_QUEEN])) :

		(pawn_attacks<SURGEWHITE>(s) & piece_bb[BLACK_PAWN]) |
		(attacks<SURGEKNIGHT>(s, occ) & piece_bb[BLACK_KNIGHT]) |
		(attacks<SURGEBISHOP>(s, occ) & (piece_bb[BLACK_BISHOP] | piece_bb[BLACK_QUEEN])) |
		(attacks<SURGEROOK>(s, occ) & (piece_bb[BLACK_ROOK] | piece_bb[BLACK_QUEEN]));
}


/*template<SurgeColor C>
SurgeBitboard ChessPosition::pinned(SurgeSquare s, SurgeBitboard us, SurgeBitboard occ) const {
	SurgeBitboard pinned = 0;

	SurgeBitboard pinners = get_xray_rook_attacks(s, occ, us) & orthogonal_sliders<~C>();
	while (pinners) pinned |= SQUARES_BETWEEN_BB[s][surge_pop_lsb(&pinners)] & us;

	pinners = get_xray_bishop_attacks(s, occ, us) & diagonal_sliders<~C>();
	while (pinners) pinned |= SQUARES_BETWEEN_BB[s][surge_pop_lsb(&pinners)] & us;

	return pinned;
}

template<SurgeColor C>
SurgeBitboard ChessPosition::blockers_to(SurgeSquare s, SurgeBitboard occ) const {
	SurgeBitboard blockers = 0;
	SurgeBitboard candidates = get_rook_attacks(s, occ) & occ;
	SurgeBitboard attackers = get_rook_attacks(s, occ ^ candidates) & orthogonal_sliders<~C>();

	candidates = get_bishop_attacks(s, occ) & occ;
	attackers |= get_bishop_attacks(s, occ ^ candidates) & diagonal_sliders<~C>();

	while (attackers) blockers |= SQUARES_BETWEEN_BB[s][surge_pop_lsb(&attackers)];
	return blockers;
}*/

//Plays a chessMove in the ChessPosition
template<SurgeColor C>
void ChessPosition::play(const ChessMove m) {
	side_to_play = ~side_to_play;
	++game_ply;
	history[game_ply] = UndoInfo(history[game_ply - 1]);

	MoveFlags type = m.flags();
	history[game_ply].entry |= SQUARE_BB[m.to()] | SQUARE_BB[m.from()];

	switch (type) {
	case QUIET:
		//The to square is guaranteed to be empty here
		move_piece_quiet(m.from(), m.to());
		break;
	case DOUBLE_PUSH:
		//The to square is guaranteed to be empty here
		move_piece_quiet(m.from(), m.to());
			
		//This is the square behind the pawn that was double-pushed
		history[game_ply].epsq = m.from() + relative_dir<C>(SURGENORTH);
		break;
	case OO:
		if (C == SURGEWHITE) {
			move_piece_quiet(e1, g1);
			move_piece_quiet(h1, f1);
		} else {
			move_piece_quiet(e8, g8);
			move_piece_quiet(h8, f8);
		}			
		break;
	case OOO:
		if (C == SURGEWHITE) {
			move_piece_quiet(e1, c1); 
			move_piece_quiet(a1, d1);
		} else {
			move_piece_quiet(e8, c8);
			move_piece_quiet(a8, d8);
		}
		break;
	case EN_PASSANT:
		move_piece_quiet(m.from(), m.to());
		remove_piece(m.to() + relative_dir<C>(SURGESOUTH));
		break;
	case PR_KNIGHT:
		remove_piece(m.from());
		put_piece(make_piece(C, SURGEKNIGHT), m.to());
		break;
	case PR_BISHOP:
		remove_piece(m.from());
		put_piece(make_piece(C, SURGEBISHOP), m.to());
		break;
	case PR_ROOK:
		remove_piece(m.from());
		put_piece(make_piece(C, SURGEROOK), m.to());
		break;
	case PR_QUEEN:
		remove_piece(m.from());
		put_piece(make_piece(C, SURGEQUEEN), m.to());
		break;
	case PC_KNIGHT:
		remove_piece(m.from());
		history[game_ply].captured = board[m.to()];
		remove_piece(m.to());
		
		put_piece(make_piece(C, SURGEKNIGHT), m.to());
		break;
	case PC_BISHOP:
		remove_piece(m.from());
		history[game_ply].captured = board[m.to()];
		remove_piece(m.to());

		put_piece(make_piece(C, SURGEBISHOP), m.to());
		break;
	case PC_ROOK:
		remove_piece(m.from());
		history[game_ply].captured = board[m.to()];
		remove_piece(m.to());

		put_piece(make_piece(C, SURGEROOK), m.to());
		break;
	case PC_QUEEN:
		remove_piece(m.from());
		history[game_ply].captured = board[m.to()];
		remove_piece(m.to());

		put_piece(make_piece(C, SURGEQUEEN), m.to());
		break;
	case CAPTURE:
		history[game_ply].captured = board[m.to()];
		move_piece(m.from(), m.to());
		
		break;
	}
}

//Undos a chessMove in the current ChessPosition, rolling it back to the previous ChessPosition
template<SurgeColor C>
void ChessPosition::undo(const ChessMove m) {
	MoveFlags type = m.flags();
	switch (type) {
	case QUIET:
		move_piece_quiet(m.to(), m.from());
		break;
	case DOUBLE_PUSH:
		move_piece_quiet(m.to(), m.from());
		break;
	case OO:
		if (C == SURGEWHITE) {
			move_piece_quiet(g1, e1);
			move_piece_quiet(f1, h1);
		} else {
			move_piece_quiet(g8, e8);
			move_piece_quiet(f8, h8);
		}
		break;
	case OOO:
		if (C == SURGEWHITE) {
			move_piece_quiet(c1, e1);
			move_piece_quiet(d1, a1);
		} else {
			move_piece_quiet(c8, e8);
			move_piece_quiet(d8, a8);
		}
		break;
	case EN_PASSANT:
		move_piece_quiet(m.to(), m.from());
		put_piece(make_piece(~C, SURGEPAWN), m.to() + relative_dir<C>(SURGESOUTH));
		break;
	case PR_KNIGHT:
	case PR_BISHOP:
	case PR_ROOK:
	case PR_QUEEN:
		remove_piece(m.to());
		put_piece(make_piece(C, SURGEPAWN), m.from());
		break;
	case PC_KNIGHT:
	case PC_BISHOP:
	case PC_ROOK:
	case PC_QUEEN:
		remove_piece(m.to());
		put_piece(make_piece(C, SURGEPAWN), m.from());
		put_piece(history[game_ply].captured, m.to());
		break;
	case CAPTURE:
		move_piece_quiet(m.to(), m.from());
		put_piece(history[game_ply].captured, m.to());
		break;
	}

	side_to_play = ~side_to_play;
	--game_ply;
}


//Generates all legal moves in a ChessPosition for the given side. Advances the chessMove pointer and returns it.
template<SurgeColor Us>
ChessMove* ChessPosition::generate_legals(ChessMove* list) {
	constexpr SurgeColor Them = ~Us;

	const SurgeBitboard us_bb = all_pieces<Us>();
	const SurgeBitboard them_bb = all_pieces<Them>();
	const SurgeBitboard all = us_bb | them_bb;

	const SurgeSquare our_king = bsf(bitboard_of(Us, SURGEKING));
	const SurgeSquare their_king = bsf(bitboard_of(Them, SURGEKING));

	const SurgeBitboard our_diag_sliders = diagonal_sliders<Us>();
	const SurgeBitboard their_diag_sliders = diagonal_sliders<Them>();
	const SurgeBitboard our_orth_sliders = orthogonal_sliders<Us>();
	const SurgeBitboard their_orth_sliders = orthogonal_sliders<Them>();

	//General purpose bitboards for attacks, masks, etc.
	SurgeBitboard b1, b2, b3;
	
	//Squares that our king cannot chessMove to
	SurgeBitboard danger = 0;

	//For each enemy piece, add all of its attacks to the danger bitboard
	danger |= pawn_attacks<Them>(bitboard_of(Them, SURGEPAWN)) | attacks<SURGEKING>(their_king, all);
	
	b1 = bitboard_of(Them, SURGEKNIGHT); 
	while (b1) danger |= attacks<SURGEKNIGHT>(surge_pop_lsb(&b1), all);
	
	b1 = their_diag_sliders;
	//all ^ SQUARE_BB[our_king] is written to prevent the king from moving to squares which are 'x-rayed'
	//by enemy bishops and queens
	while (b1) danger |= attacks<SURGEBISHOP>(surge_pop_lsb(&b1), all ^ SQUARE_BB[our_king]);
	
	b1 = their_orth_sliders;
	//all ^ SQUARE_BB[our_king] is written to prevent the king from moving to squares which are 'x-rayed'
	//by enemy rooks and queens
	while (b1) danger |= attacks<SURGEROOK>(surge_pop_lsb(&b1), all ^ SQUARE_BB[our_king]);

	//The king can chessMove to all of its surrounding squares, except ones that are attacked, and
	//ones that have our own pieces on them
	b1 = attacks<SURGEKING>(our_king, all) & ~(us_bb | danger);
	list = make<QUIET>(our_king, b1 & ~them_bb, list);
	list = make<CAPTURE>(our_king, b1 & them_bb, list);

	//The capture mask filters destination squares to those that contain an enemy piece that is checking the 
	//king and must be captured
	SurgeBitboard capture_mask,
	
	//The quiet mask filter destination squares to those where pieces must be moved to block an incoming attack 
	//to the king
	 quiet_mask;
	
	//A general purpose square for storing destinations, etc.
	SurgeSquare s;

	//Checkers of each piece type are identified by:
	//1. Projecting attacks FROM the king square
	//2. Intersecting this bitboard with the enemy bitboard of that piece type
	checkers = attacks<SURGEKNIGHT>(our_king, all) & bitboard_of(Them, SURGEKNIGHT)
		| pawn_attacks<Us>(our_king) & bitboard_of(Them, SURGEPAWN);
	
	//Here, we identify slider checkers and pinners simultaneously, and candidates for such pinners 
	//and checkers are represented by the bitboard <candidates>
	SurgeBitboard candidates = attacks<SURGEROOK>(our_king, them_bb) & their_orth_sliders
		| attacks<SURGEBISHOP>(our_king, them_bb) & their_diag_sliders;

	pinned = 0;
	while (candidates) {
		s = surge_pop_lsb(&candidates);
		b1 = SQUARES_BETWEEN_BB[our_king][s] & us_bb;
		
		//Do the squares in between the enemy slider and our king contain any of our pieces?
		//If not, add the slider to the checker bitboard
		if (b1 == 0) checkers ^= SQUARE_BB[s];
		//If there is only one of our pieces between them, add our piece to the pinned bitboard 
		else if ((b1 & b1 - 1) == 0) pinned ^= b1;
	}

	//This makes it easier to mask pieces
	const SurgeBitboard not_pinned = ~pinned;

	switch (sparse_pop_count(checkers)) {
	case 2:
		//If there is a double check, the only legal moves are king moves out of check
		return list;
	case 1: {
		//It's a single check!
		
		SurgeSquare checker_square = bsf(checkers);

		switch (board[checker_square]) {
		case make_piece(Them, SURGEPAWN):
			//If the checker is a pawn, we must check for e.p. moves that can capture it
			//This evaluates to true if the checking piece is the one which just double pushed
			if (checkers == shift<relative_dir<Us>(SURGESOUTH)>(SQUARE_BB[history[game_ply].epsq])) {
				//b1 contains our pawns that can capture the checker e.p.
				b1 = pawn_attacks<Them>(history[game_ply].epsq) & bitboard_of(Us, SURGEPAWN) & not_pinned;
				while (b1) *list++ = ChessMove(surge_pop_lsb(&b1), history[game_ply].epsq, EN_PASSANT);
			}
			//FALL THROUGH INTENTIONAL
		case make_piece(Them, SURGEKNIGHT):
			//If the checker is either a pawn or a knight, the only legal moves are to capture
			//the checker. Only non-pinned pieces can capture it
			b1 = attackers_from<Us>(checker_square, all) & not_pinned;
			while (b1) *list++ = ChessMove(surge_pop_lsb(&b1), checker_square, CAPTURE);

			return list;
		default:
			//We must capture the checking piece
			capture_mask = checkers;
			
			//...or we can block it since it is guaranteed to be a slider
			quiet_mask = SQUARES_BETWEEN_BB[our_king][checker_square];
			break;
		}

		break;
	}

	default:
		//We can capture any enemy piece
		capture_mask = them_bb;
		
		//...and we can play a quiet chessMove to any square which is not occupied
		quiet_mask = ~all;

		if (history[game_ply].epsq != NO_SQUARE) {
			//b1 contains our pawns that can perform an e.p. capture
			b2 = pawn_attacks<Them>(history[game_ply].epsq) & bitboard_of(Us, SURGEPAWN);
			b1 = b2 & not_pinned;
			while (b1) {
				s = surge_pop_lsb(&b1);
				
				//This piece of evil bit-fiddling magic prevents the infamous 'pseudo-pinned' e.p. case,
				//where the pawn is not directly pinned, but on moving the pawn and capturing the enemy pawn
				//e.p., a rook or queen attack to the king is revealed
				
				/*
				.nbqkbnr
				ppp.pppp
				........
				r..pP..K
				........
				........
				PPPP.PPP
				RNBQ.BNR
				
				Here, if white plays exd5 e.p., the black rook on a5 attacks the white king on h5 
				*/
				
				if ((sliding_attacks(our_king, all ^ SQUARE_BB[s]
					^ shift<relative_dir<Us>(SURGESOUTH)>(SQUARE_BB[history[game_ply].epsq]),
					MASK_RANK[rank_of(our_king)]) &
					their_orth_sliders) == 0)
						*list++ = ChessMove(s, history[game_ply].epsq, EN_PASSANT);
			}
			
			//Pinned pawns can only capture e.p. if they are pinned diagonally and the e.p. square is in line with the king 
			b1 = b2 & pinned & LINE[history[game_ply].epsq][our_king];
			if (b1) {
				*list++ = ChessMove(bsf(b1), history[game_ply].epsq, EN_PASSANT);
			}
		}

		//Only add castling if:
		//1. The king and the rook have both not moved
		//2. No piece is attacking between the the rook and the king
		//3. The king is not in check
		if (!((history[game_ply].entry & oo_mask<Us>()) | ((all | danger) & oo_blockers_mask<Us>())))
			*list++ = Us == SURGEWHITE ? ChessMove(e1, h1, OO) : ChessMove(e8, h8, OO);
		if (!((history[game_ply].entry & ooo_mask<Us>()) |
			((all | (danger & ~ignore_ooo_danger<Us>())) & ooo_blockers_mask<Us>())))
			*list++ = Us == SURGEWHITE ? ChessMove(e1, c1, OOO) : ChessMove(e8, c8, OOO);

		//For each pinned rook, bishop or queen...
		b1 = ~(not_pinned | bitboard_of(Us, SURGEKNIGHT));
		while (b1) {
			s = surge_pop_lsb(&b1);
			
			//...only include attacks that are aligned with our king, since pinned pieces
			//are constrained to chessMove in this direction only
			b2 = attacks(type_of(board[s]), s, all) & LINE[our_king][s];
			list = make<QUIET>(s, b2 & quiet_mask, list);
			list = make<CAPTURE>(s, b2 & capture_mask, list);
		}

		//For each pinned pawn...
		b1 = ~not_pinned & bitboard_of(Us, SURGEPAWN);
		while (b1) {
			s = surge_pop_lsb(&b1);

			if (rank_of(s) == relative_rank<Us>(RANK7)) {
				//Quiet promotions are impossible since the square in front of the pawn will
				//either be occupied by the king or the pinner, or doing so would leave our king
				//in check
				b2 = pawn_attacks<Us>(s) & capture_mask & LINE[our_king][s];
				list = make<PROMOTION_CAPTURES>(s, b2, list);
			}
			else {
				b2 = pawn_attacks<Us>(s) & them_bb & LINE[s][our_king];
				list = make<CAPTURE>(s, b2, list);
				
				//Single pawn pushes
				b2 = shift<relative_dir<Us>(SURGENORTH)>(SQUARE_BB[s]) & ~all & LINE[our_king][s];
				//Double pawn pushes (only pawns on rank 3/6 are eligible)
				b3 = shift<relative_dir<Us>(SURGENORTH)>(b2 &
					MASK_RANK[relative_rank<Us>(RANK3)]) & ~all & LINE[our_king][s];
				list = make<QUIET>(s, b2, list);
				list = make<DOUBLE_PUSH>(s, b3, list);
			}
		}
		
		//Pinned knights cannot chessMove anywhere, so we're done with pinned pieces!

		break;
	}

	//Non-pinned knight moves
	b1 = bitboard_of(Us, SURGEKNIGHT) & not_pinned;
	while (b1) {
		s = surge_pop_lsb(&b1);
		b2 = attacks<SURGEKNIGHT>(s, all);
		list = make<QUIET>(s, b2 & quiet_mask, list);
		list = make<CAPTURE>(s, b2 & capture_mask, list);
	}

	//Non-pinned bishops and queens
	b1 = our_diag_sliders & not_pinned;
	while (b1) {
		s = surge_pop_lsb(&b1);
		b2 = attacks<SURGEBISHOP>(s, all);
		list = make<QUIET>(s, b2 & quiet_mask, list);
		list = make<CAPTURE>(s, b2 & capture_mask, list);
	}

	//Non-pinned rooks and queens
	b1 = our_orth_sliders & not_pinned;
	while (b1) {
		s = surge_pop_lsb(&b1);
		b2 = attacks<SURGEROOK>(s, all);
		list = make<QUIET>(s, b2 & quiet_mask, list);
		list = make<CAPTURE>(s, b2 & capture_mask, list);
	}

	//b1 contains non-pinned pawns which are not on the last rank
	b1 = bitboard_of(Us, SURGEPAWN) & not_pinned & ~MASK_RANK[relative_rank<Us>(RANK7)];
	
	//Single pawn pushes
	b2 = shift<relative_dir<Us>(SURGENORTH)>(b1) & ~all;
	
	//Double pawn pushes (only pawns on rank 3/6 are eligible)
	b3 = shift<relative_dir<Us>(SURGENORTH)>(b2 & MASK_RANK[relative_rank<Us>(RANK3)]) & quiet_mask;
	
	//We & this with the quiet mask only later, as a non-check-blocking single push does NOT mean that the 
	//corresponding double push is not blocking check either.
	b2 &= quiet_mask;

	while (b2) {
		s = surge_pop_lsb(&b2);
		*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH), s, QUIET);
	}

	while (b3) {
		s = surge_pop_lsb(&b3);
		*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH_NORTH), s, DOUBLE_PUSH);
	}

	//Pawn captures
	b2 = shift<relative_dir<Us>(SURGENORTH_WEST)>(b1) & capture_mask;
	b3 = shift<relative_dir<Us>(SURGENORTH_EAST)>(b1) & capture_mask;

	while (b2) {
		s = surge_pop_lsb(&b2);
		*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH_WEST), s, CAPTURE);
	}

	while (b3) {
		s = surge_pop_lsb(&b3);
		*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH_EAST), s, CAPTURE);
	}

	//b1 now contains non-pinned pawns which ARE on the last rank (about to promote)
	b1 = bitboard_of(Us, SURGEPAWN) & not_pinned & MASK_RANK[relative_rank<Us>(RANK7)];
	if (b1) {
		//Quiet promotions
		b2 = shift<relative_dir<Us>(SURGENORTH)>(b1) & quiet_mask;
		while (b2) {
			s = surge_pop_lsb(&b2);
			//One chessMove is added for each promotion piece
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH), s, PR_KNIGHT);
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH), s, PR_BISHOP);
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH), s, PR_ROOK);
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH), s, PR_QUEEN);
		}

		//Promotion captures
		b2 = shift<relative_dir<Us>(SURGENORTH_WEST)>(b1) & capture_mask;
		b3 = shift<relative_dir<Us>(SURGENORTH_EAST)>(b1) & capture_mask;

		while (b2) {
			s = surge_pop_lsb(&b2);
			//One chessMove is added for each promotion piece
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH_WEST), s, PC_KNIGHT);
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH_WEST), s, PC_BISHOP);
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH_WEST), s, PC_ROOK);
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH_WEST), s, PC_QUEEN);
		}

		while (b3) {
			s = surge_pop_lsb(&b3);
			//One chessMove is added for each promotion piece
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH_EAST), s, PC_KNIGHT);
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH_EAST), s, PC_BISHOP);
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH_EAST), s, PC_ROOK);
			*list++ = ChessMove(s - relative_dir<Us>(SURGENORTH_EAST), s, PC_QUEEN);
		}
	}

	return list;
}

//A convenience class for interfacing with legal moves, rather than using the low-level
//generate_legals() function directly. It can be iterated over.
template<SurgeColor Us>
class SurgeMoveList {
public:
	explicit SurgeMoveList(ChessPosition& p) : last(p.generate_legals<Us>(list)) {}

	const ChessMove* begin() const { return list; }
	const ChessMove* end() const { return last; }
	size_t size() const { return last - list; }
private:
	ChessMove list[218];
	ChessMove *last;
};
