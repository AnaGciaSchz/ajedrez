// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "surge/src/chessPosition.h"
#include "surge/src/tables.h"
#include "surge/src/types.h"

#include "Stockfish-sf_12/src/uci.h"
#include "Stockfish-sf_12/src/bitboard.h"
#include "Stockfish-sf_12/src/endgame.h"
#include "Stockfish-sf_12/src/position.h"
#include "Stockfish-sf_12/src/search.h"
#include "Stockfish-sf_12/src/thread.h"
#include "Stockfish-sf_12/src/tt.h"
#include "Stockfish-sf_12/src/uci.h"
#include "Stockfish-sf_12/src/syzygy/tbprobe.h"

#include <chrono>
#include <thread>

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Misc/InteractiveProcess.h"
#include "Misc/Paths.h"
#include "MyBlueprintFunctionLibrary.generated.h"

using namespace std;

/**
 * 
 */
UCLASS()
class AJEDREZ_API UMyBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UMyBlueprintFunctionLibrary();

	UFUNCTION(BlueprintCallable, Category = "Chess Function Library")
	static FString GetInitialMoves();

	UFUNCTION(BlueprintCallable, Category = "Chess Function Library")
	static FString GetMovesFor(int color, FString position);

	UFUNCTION(BlueprintCallable, Category = "Chess Function Library")
	static FString StockFishMove(FString position);

	UFUNCTION(BlueprintCallable, Category = "Chess Function Library")
	static FString UpdateFEN(FString initialFEN, FString move);

	UFUNCTION(BlueprintCallable, Category = "Chess Function Library")
	static FString GetPieceColor(FString currentFEN, FString coord);

	static vector<string> split(const string& s, char delim);
};
